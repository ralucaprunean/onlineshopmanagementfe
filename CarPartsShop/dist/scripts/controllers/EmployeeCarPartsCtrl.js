app
.controller("EmployeeCarPartsCtrl", function($scope, $location, $state) {

  readParts();
	
  $scope.addPart = function(){
    var part = {};
    part.Name = $scope.Name;
    part.description = $scope.description;
    part.quantity = $scope.quantity;
    part.price = $scope.price;
    $.ajax({
      url:"http://localhost/OnlineShop/api/controllers/products/AddNewProduct",
        data: part,
        dataType: 'JSON',
        method: 'POST',
        success:function(response, textStatus, xhr){
            alert('Product added!');
            readParts();
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('The part could not be added!');
        }
    })
  }
  
   function readParts(){
    $.ajax({
      url:"http://localhost/OnlineShop/api/controllers/products/GetProducts",
        type:"GET",
        contentType:"application/json",   
        success:function(response, textStatus, xhr){
            $scope.parts = response;
            $scope.$apply(); 
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('Server error');
        }
    })
  }
   $scope.updateCarPart = function(part_id, selected_price){
    var data = {};
    data.id = part_id;
    data.price = selected_price;
    $.ajax({
      url:"http://localhost/OnlineShop/api/controllers/Products/EditProductDetails",
        data: data,
        dataType: 'JSON',
        method: 'POST',
        success:function(response, textStatus, xhr){
            alert('Product updated!');
            readParts();
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('The part could not be updated!');
        }
    })
  }
	
	  $scope.deleteCarPart = function (part_id) {
    var data = {};
    data.id = part_id;
    data.userId = sessionStorage.Id;
    console.log(data.userId)
    $.ajax({
      url: "http://localhost/OnlineShop/api/controllers/Products/DeleteProduct",
      data: data,
      dataType: 'JSON',
      method: 'POST',
        success:function(response, textStatus, xhr){
            readParts();
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('The part could not be deleted!');
        }
    })
  }
})
