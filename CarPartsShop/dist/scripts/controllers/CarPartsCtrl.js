app
.controller("CarPartsCtrl", function($scope, $location, $state) {

	$.ajax({
      url:"http://localhost/OnlineShop/api/controllers/products/GetProducts",
      type:"GET",
      contentType:"application/json",   
      success:function(response, textStatus, xhr){
         	$scope.parts = response;
         	$scope.$apply(); 
      },
      error: function(error){
          console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
        	alert('Server error');
      }
  })

})