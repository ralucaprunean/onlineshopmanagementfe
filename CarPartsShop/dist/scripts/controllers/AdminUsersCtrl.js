app
.controller("AdminUsersCtrl", function($scope, $location, $state) {

  readUsers();

  function readUsers(){
    $.ajax({
        url:"http://localhost/OnlineShop/api/controllers/user/GetUsers",
        type:"GET",
        contentType:"application/json",   
        success:function(response, textStatus, xhr){
            $scope.users = response;
            $scope.$apply(); 
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('Server error');
        }
    })
  }
  
     $scope.addUser = function() {
    		var user = {};
    		user.username = $scope.username;
    		user.password = $scope.password;
    		user.role = $scope.role;
    		user.email = $scope.email;
    		user.firstName = $scope.firstName;
			user.lastName = $scope.lastName;
            console.log(user);
	        $.ajax({
              url: "http://localhost/OnlineShop/api/controllers/user/AddNewUser", 
				data: user,
				dataType: 'JSON',
				method: 'POST',
				success: function(data) {
					console.log(data);	
					alert('User Added!');
					readUsers();
				}, error: function(jqXHRx, ajaxSettings, thrownError) {
					alert("Server Error");
				}
			})
	}
  
  $scope.deleteUser = function(user_id){
	  var data = {};
	  data.Id = user_id;
	  console.log(data);
    $.ajax({
        url:"http://localhost/OnlineShop/api/controllers/user/DeleteUser",
		data: data,
        dataType: 'JSON',
		method: 'POST',
        success:function(response, textStatus, xhr){
            readUsers(); 
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('The user could not be deleted!');
        }
    })
  }

    $scope.updateUser = function(){
    var user = {};

    user.Id = $scope.user_id1;
    user.firstName = $scope.user_name1;
	user.lastName = $scope.lastname;
	user.Email = $scope.user_email1;
	user.Username = $scope.username;
	user.Password = $scope.user_password1;
	user.Role = $scope.user_role1;

    $.ajax({
        url:"http://localhost/OnlineShop/api/controllers/user/EditUserDetails",
        data: user,
        dataType: 'JSON',
        method: 'POST',
        success:function(response, textStatus, xhr){
            alert('User updated!');
            readUsers();
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('The user could not be updated!');
        }
    })
  }

})