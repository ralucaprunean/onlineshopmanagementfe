app
.controller("PartCtrl", function($scope, $location, $state, $stateParams) {
	var part_id = $stateParams.part_id;
	var data = {};
	data.Id = $stateParams.part_id;
	console.log(data);
	$.ajax({
        url:"http://localhost/OnlineShop/api/controllers/products/GetProduct",
		data: data,
        dataType: 'JSON',
		method: 'POST',   
        success:function(response, textStatus, xhr){
           	$scope.part = response;
           	$scope.$apply(); 
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
          	alert('Server error');
        }
    });

    $("#add-to-cart-button").on('click',function(){

    	var data = {};
    	data.userId = sessionStorage.Id;
    	data.Id = part_id;

    	console.log(data);

    	$.ajax({
	        url:"http://localhost/OnlineShop/api/controllers/orderitems/AddNewProductToOrder",
			data: data,
			dataType: 'JSON',
			method: 'POST',  
	        success:function(response, textStatus, xhr){
	        	alert('The product was added to your cart!');
	           	console.log(response);
	        },
	        error: function(error){
	            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
	          	alert('Server error');
	        }
	    });
    })
})