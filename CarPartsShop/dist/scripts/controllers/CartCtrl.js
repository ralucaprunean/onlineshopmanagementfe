app
.controller("CartCtrl", function($scope, $location, $state, $stateParams) {

	var user_id = sessionStorage.Id;

	readCartFromService();
	
	$scope.removeFromCart =function(cartItemId){
  		console.log(cartItemId);
		var data = {};
		data.id = cartItemId;
		data.userId = user_id;
  		$.ajax({
	      	url:"http://localhost/OnlineShop/api/controllers/orderitems/DeleteProductFromOrder",
	        data: data,
			dataType: 'JSON',
			method: 'POST', 
	      	success:function(response, textStatus, xhr){
				readCartFromService();
	      	},
	      	error: function(error){
	          	console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
	        	alert('Server error');
	      	}
	  	});
  	}
	
  	function readCartFromService(){
		var data = {};
		data.id = user_id;
		$.ajax({
	      	url:"http://localhost/OnlineShop/api/controllers/orderitems/GetOrderItemForUser",
	      	data: data,
			dataType: 'JSON',
			method: 'POST',    
	      	success:function(response, textStatus, xhr){
				console.log(response);
	      		/*var partsResponse = [];
	      		for (var i = 0 ; i < response.length; i++){
	      			partsResponse[i] = response[i].ProductName;
	      			partsResponse[i].cartItemId = response[i].user_part_id;
	      		}
	      		console.log(partsResponse);*/
	         	$scope.parts = response;
	         	$scope.$apply(); 
	      	},
	      	error: function(error){
	          	console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
	        	alert('Server error');
	      	}
	  	});
	}
	
	$scope.finishOrder = function(){

		var data ={};
		data.Id = sessionStorage.Id;
  		$.ajax({
	      	url:"http://localhost/OnlineShop/api/controllers/order/SendEmail",
	      	data: data,
			dataType: 'JSON',
			method: 'POST',  
	      	success:function(response, textStatus, xhr){
				readCartFromService();
				alert('Ati finalizat comanda cu succes.');
	      	},
	      	error: function(error){
	          	console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
	        	alert('Server error');
	      	}
	  	});
  	}

})