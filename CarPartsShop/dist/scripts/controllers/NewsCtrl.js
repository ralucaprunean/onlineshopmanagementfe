app
.controller("NewsCtrl", function($scope, $location, $state) {

    $.ajax({
        url:"http://localhost/OnlineShop/api/controllers/News/GetNews",
        type:"GET",
        contentType:"application/json",   
        success:function(response, textStatus, xhr){
            $scope.news = response;
            $scope.$apply(); 
        },
        error: function(error){
            console.log('Error! status: ' + error.status + '. statusText: ' + error.statusText);
            alert('Server error');
        }
    })
  
})