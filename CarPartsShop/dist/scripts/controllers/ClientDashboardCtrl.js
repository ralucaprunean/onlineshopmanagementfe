app

.controller("ClientDashboardCtrl", function($scope, $state, $state) 
{ 
	$scope.name = sessionStorage.firstName + " " + sessionStorage.lastName;

	$scope.logoutAction = function(){
		$state.go('login');
		sessionStorage.clear();
	}

	if (sessionStorage.role != "client"){
		$state.go('login');
	}
});
