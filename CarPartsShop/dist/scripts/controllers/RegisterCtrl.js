app
.controller("RegisterCtrl", function($scope, $location, $state) {
    $scope.submit = function() {
    		var user = {};
    		user.username = $scope.username;
    		user.password = $scope.password;
    		user.role = $scope.role;
    		user.email = $scope.email;
    		user.firstName = $scope.firstName;
			user.lastName = $scope.lastName;
            console.log(user);
	        $.ajax({
              url: "http://localhost/OnlineShop/api/controllers/user/AddNewUser", 
				data: user,
				dataType: 'JSON',
				method: 'POST',
				success: function(data) {
					console.log(data);	
					alert('User Registered!');
					$state.go("login");
				}, error: function(jqXHRx, ajaxSettings, thrownError) {
					alert("Server Error");
				}
			})
	}
})
