app
.controller("LoginCtrl", function($scope, $location, $state) {
    $scope.submit = function() {
        
        var object = {};
        object.username = $scope.username;
        object.password = $scope.password;

        $.ajax({
          url: "http://localhost/OnlineShop/api/controllers/User/LogIn", 
			data: object,
			dataType: 'JSON',
			method: 'POST',
			success: function(data) {
				console.log(data);
				if (data.login == "success"){
					if (data.role == "admin"){
						sessionStorage.username = $scope.username;
						sessionStorage.password = $scope.password;
						sessionStorage.Id = data.Id;
						sessionStorage.firstName = data.firstName;
						sessionStorage.lastName = data.lastName;
						sessionStorage.role = data.role;
						$state.go('admindashboard');	
					} else if (data.role == "client"){
						sessionStorage.username = $scope.username;
						sessionStorage.password = $scope.password;
						sessionStorage.Id = data.Id;
						sessionStorage.firstName = data.firstName;
						sessionStorage.lastName = data.lastName;
						sessionStorage.role = data.role;
						$state.go('clientdashboard');
					} else if (data.role == "employee"){
						sessionStorage.username = $scope.username;
						sessionStorage.password = $scope.password;
						sessionStorage.Id = data.Id;
						sessionStorage.firstName = data.firstName;
						sessionStorage.lastName = data.lastName;
						sessionStorage.role = data.role;
						$state.go('employeedashboard');
					}
					
				} else {
					alert("Incorrect username or password");
				}
				
			}, error: function(jqXHRx, ajaxSettings, thrownError) {
				alert("Server Error");
			}
		})
	}

	$("#register-button").on('click',function(){
		$state.go("register");
	})
})
