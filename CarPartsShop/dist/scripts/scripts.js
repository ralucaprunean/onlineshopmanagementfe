"use strict";

var app = angular.module("myapp", ["ui.router", "ngAnimate"]);

app.config(["$stateProvider", "$urlRouterProvider", function(r, t) {

    t.otherwise("/login"),

    r
    .state("base", 
    	{ 
    		abstract: true,
    		url: "", 
    		templateUrl: "views/base.html" 
    	})
    .state("login", 
    	{ 
    		url: "/login", 
    		parent: "base", 
    		templateUrl: "views/login.html", 
    		controller: "LoginCtrl" 
    	})
    .state("register", 
        { 
            url: "/register", 
            parent: "base", 
            templateUrl: "views/register.html", 
            controller: "RegisterCtrl" 
        })
    .state("dashboard", 
    	{ 
    		url: "/dashboard", 
    		parent: "base", 
    		templateUrl: "views/dashboard.html", 
    		controller: "DashboardCtrl" 
    	})
    .state("admindashboard", 
        { 
            url: "/admindashboard", 
            parent: "base", 
            templateUrl: "views/admindashboard.html", 
        })
		
	.state("clientdashboard", 
        { 
            url: "/clientdashboard", 
            parent: "base", 
            templateUrl: "views/clientdashboard.html",   
			controller: "ClientDashboardCtrl" 			
    })
	.state("employeedashboard", 
        { 
            url: "/employeedashboard", 
            parent: "base", 
            templateUrl: "views/employeedashboard.html",     
    })
    .state("employeecarparts", 
        { 
            url: "/employeecarparts", 
            parent: "employeedashboard", 
            templateUrl: "views/employeedashboard/employee-carparts.html",
            controller: "EmployeeCarPartsCtrl"
        })
	 .state("employeenews", 
        { 
            url: "/employeenews", 
            parent: "employeedashboard", 
            templateUrl: "views/employeedashboard/employee-news.html",
            controller: "EmployeeNewsCtrl"
        })		
    .state("adminusers", 
        { 
            url: "/adminusers", 
            parent: "admindashboard",
            templateUrl: "views/admindashboard/admin-users.html",
            controller: "AdminUsersCtrl"
        })
    .state("carparts", 
    	{ 
    		url: "/carparts", 
    		parent: "clientdashboard", 
    		templateUrl: "views/clientdashboard/carparts.html",
            controller: "CarPartsCtrl"
    	})
    .state("part", 
    	{ 
    		url: "/part/:part_id", 
    		parent: "clientdashboard", 
    		templateUrl: "views/clientdashboard/part.html" ,
            controller: "PartCtrl"
    	})
    .state("cart", 
    	{ 
    		url: "/cart", 
    		parent: "clientdashboard", 
    		templateUrl: "views/clientdashboard/cart.html" ,
            controller: "CartCtrl"
    	})
	.state("news", 
    	{ 
    		url: "/news", 
    		parent: "clientdashboard", 
    		templateUrl: "views/clientdashboard/news.html" ,
            controller: "NewsCtrl"
    	})
    .state("orders", 
        { 
            url: "/orders", 
            parent: "dashboard", 
            templateUrl: "views/dashboard/orders.html" ,
            controller: "OrdersCtrl"
        })
}]);